## ADE CLION

- This project generates an CLION (https://www.jetbrains.com/clion/) volume for [`ade`](https://gitlab.com/ApexAI/ade-cli)

### How to use

- In the `.aderc` file add, `registry.gitlab.com/amadeuszsz/ade-clion/foxy:latest` to the list of `ADE_IMAGES`: e.g.

```
export ADE_IMAGES="
  registry.gitlab.com/group/project/ade:latest
  registry.gitlab.com/amadeuszsz/ade-clion/foxy:latest
"
```

### Getting a specific CLION version

- Generating a volume for a different CLION version is as simple as creating a git tag (e.g `2021.3.3`)
- [Create an issue](https://gitlab.com/amadeuszsz/ade-clion/issues/new), if the desired version is not available 
